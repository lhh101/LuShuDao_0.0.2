package com.lixiaohan.controller;


import com.lixiaohan.pojo.Result;
import com.lixiaohan.utils.OSSUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
//@CrossOrigin//跨域
@RequestMapping("/OSS")
public class OssController {

    /**
     * 上传用户头像
     * @param
     * @return
     */
    @RequestMapping(value = "/uploadUserHeadImg",method = RequestMethod.POST)
    public Result uploadUserHeadImg(MultipartFile uploadFile) {
        try {
            OSSUtil ossUtil = new OSSUtil();
            String url = ossUtil.uploadDocument(uploadFile,"userHeadImg");
            System.out.println("路径："+url);
            return Result.ok().message("头像添加成功2").data("url",url);
        }catch (Exception e){
            return Result.error().message("上传头像失败2").data("文件名:",uploadFile.getOriginalFilename());
        }

    }

    /**
     * 用户上传空间图片
     * @param
     * @return
     */
    @RequestMapping(value = "/uploadUserSpaceImg",method = RequestMethod.POST)
    public Result uploadUserSpaceImg(MultipartFile uploadFile) {
        try {
            OSSUtil ossUtil = new OSSUtil();
            String url = ossUtil.uploadDocument(uploadFile,"userSpaceImg");
            System.out.println("路径："+url);
            return Result.ok().message("空间图片添加成功2").data("url",url);
        }catch (Exception e){
            return Result.error().message("上传图片失败2").data("exception",e);
        }
    }


}
