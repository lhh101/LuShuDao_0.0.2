import org.junit.Test;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestGetView {
    @Test
    public void testView1(){
        String htmlStr = "<h1>青城 日子</h1><br><img src=\"http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userSpaceImg/20220322/434e599644974ee3814c65ea6d280d11.jpg\" alt=\"cat\" width=\"390&quot;&quot;\"><h2>123</h2><img src=\"http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userSpaceImg/20220322/434e599644974ee3814c65ea6d280d66.jpg\" alt=\"cat\" width=\"390&quot;&quot;\">" +
                "<h1>青城 日子</h1><br><img src=\"http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userSpaceImg/20220322/434e599644974ee3814c65ea6d280d33.jpg\" alt=\"cat\" width=\"390&quot;&quot;\"><h2>123</h2><img src=\"http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userSpaceImg/20220322/434e599644974ee3814c65ea6d280d99.jpg\" alt=\"cat\" width=\"390&quot;&quot;\"><h1>青城 日子</h1><br><img src=\"http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userSpaceImg/20220322/434e599644974ee3814c65ea6d280d77.jpg\" alt=\"cat\" width=\"390&quot;&quot;\"><h2>123</h2>" +
                "<img src=\"http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userSpaceImg/20220322/434e599644974ee3814c65ea6d280d22.jpg\" alt=\"cat\" width=\"390&quot;&quot;\">";
        //<h1>青城 日子</h1><br><img src="http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userSpaceImg/20220322/434e599644974ee3814c65ea6d280d11.jpg" alt="cat" width="390&quot;&quot;">
        Set<String> pics = new HashSet<>();
        String img = "";
        Pattern p_image;
        Matcher m_image;
        //     String regEx_img = "<img.*src=(.*?)[^>]*?>"; //图片链接地址
        String regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>";
        p_image = Pattern.compile
                (regEx_img, Pattern.CASE_INSENSITIVE);
        m_image = p_image.matcher(htmlStr);
        while (m_image.find()) {
            // 得到<img />数据
            img = m_image.group();
            // 匹配<img>中的src数据
            Matcher m = Pattern.compile("src\\s*=\\s*\"?(.*?)(\"|>|\\s+)").matcher(img);
            while (m.find()) {
                pics.add(m.group(1));
            }
        }
//        pics.forEach(System.out::println);
//        List list = new ArrayList(pics);
//        String[] urls = (String[]) pics.toArray(new String[pics.size()]);
//        for (String url : urls) {
//            System.out.println(url);
//        }
        List newUrls = new ArrayList();
        String[] urls = (String[]) pics.toArray(new String[pics.size()]);

        System.out.println(pics.size());
        if(pics.size()>3){
            newUrls.addAll(Arrays.asList(urls).subList(0, 3));
        }
        newUrls.forEach(System.out::println);



    }


    @Test
    public void test2(){
        List<Integer> list= new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        ListIterator<Integer> integerListIterator = list.listIterator(list.size());
        if(integerListIterator.hasPrevious()){
            Integer pint = integerListIterator.previous();
            System.out.println(pint);
        }


    }
}
