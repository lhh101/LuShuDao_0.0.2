import com.lixiaohan.lushudao.dao.PictureSpaceInfoMapper;
import com.lixiaohan.lushudao.dao.RoamPictureMapper;
import com.lixiaohan.lushudao.pojo.PictureSpaceInfo;
import com.lixiaohan.lushudao.pojo.RoamPicture;
import com.lixiaohan.lushudao.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.UUID;

public class BastTest {
    @Test
    public void testUUID(){
        System.out.println(UUID.randomUUID()); // 4

    }

    @Test
    public void testReturen(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        RoamPictureMapper mapper = sqlSession.getMapper(RoamPictureMapper.class);
        RoamPicture roamPicture = new RoamPicture();
        roamPicture.setPictureUrl("url3");
        roamPicture.setUserId(2);
        Integer integer = mapper.addOnePicture(roamPicture);
        System.out.println(integer);
        System.out.println(roamPicture.getPictureId());
        sqlSession.close();
    }

    @Test
    public void  addSpacePicture(){
        String url="url4";
        Integer uid=1;
        Integer sid=1;
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        RoamPictureMapper mapperRoamPicture = sqlSession.getMapper(RoamPictureMapper.class);
        PictureSpaceInfoMapper mapperSpacePicture = sqlSession.getMapper(PictureSpaceInfoMapper.class);
        RoamPicture roamPicture = new RoamPicture();
        roamPicture.setPictureUrl(url);
        roamPicture.setUserId(uid);
        Integer integer = mapperRoamPicture.addOnePicture(roamPicture);
        System.out.println("成功为1失败为0:"+integer);
        Integer pictureId = roamPicture.getPictureId();

        PictureSpaceInfo pictureSpaceInfo = new PictureSpaceInfo();
        pictureSpaceInfo.setPictureId(pictureId);
        pictureSpaceInfo.setSpaceId(sid);
        Integer flag = mapperSpacePicture.addSpacePicture(pictureSpaceInfo);
        System.out.println("成功为1失败为0:"+flag);
        sqlSession.close();

    }
}
