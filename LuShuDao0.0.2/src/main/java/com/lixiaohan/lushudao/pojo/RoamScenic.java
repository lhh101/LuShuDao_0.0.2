package com.lixiaohan.lushudao.pojo;

import lombok.Data;

@Data
public class RoamScenic {
    private Integer scenicId;//景点编号(N)
    private String scenicName;//景点标题(N)
}
