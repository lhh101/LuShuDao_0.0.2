package com.lixiaohan.lushudao.pojo;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class UsGoodInfo {
    private Integer usGoodId;//空间信息点赞表id
    private Integer userId;//用户id
    private Timestamp createTime;//点赞时间
    private Integer spaceId;//空间信息id
    private Integer status;//状态（0表示未点赞，1表示点赞过）
}
