package com.lixiaohan.lushudao.pojo;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class UserSpace {
    private String spaceId;//个人空间id(N)
    private String spaceContent;//个人空间内容(N)
    private Integer spaceGood;//个人空间点赞数
    private Timestamp spaceTime;//个人空间发布时间(N)
    private Integer userId;//发布的用户id(N)
    private Integer pubUser;//是否公开(N)
}
