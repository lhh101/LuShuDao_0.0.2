package com.lixiaohan.lushudao.pojo;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class RoamSpaceComment {
    private Integer spaceCommnetId;//评论编号(N)
    private Timestamp spaceCommentTime;//评论时间(N)
    private String spaceCommentContent;//评论内容(N)
    private Integer spaceCommentGood;//点赞数(N)
    private Integer spaceId;//个人空间信息编号(N)
    private Integer userId;//评论用户编号(N)

}
