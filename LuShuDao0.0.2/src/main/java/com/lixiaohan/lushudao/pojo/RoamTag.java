package com.lixiaohan.lushudao.pojo;

import lombok.Data;

@Data
public class RoamTag {
    private Integer tagId;//tag编号(N)
    private String tagText;//tag内容(N)
}
