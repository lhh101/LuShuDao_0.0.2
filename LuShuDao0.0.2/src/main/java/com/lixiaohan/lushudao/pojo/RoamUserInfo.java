package com.lixiaohan.lushudao.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

/**
 * 用户信息表
 */
@Data
@AllArgsConstructor//只加有参构造
@NoArgsConstructor//补上无参构造
public class RoamUserInfo {
    private Integer userId;//用户编号(N)
    private String userAcc;//用户账户(N)
    private String userPwd;//用户密码(N)
    private String userName;//昵称
    private String userChara;//虚拟人物图片地址(N)
    private String userSpirit;//景点精灵图片地址(N)
    private String userHead;//用户头像地址
    private String userAccSelect;//用户登录的账户选择
    private Timestamp userFirstTime;//用户注册时间(N)
    private Integer characterSelect;//用户虚拟人物选择
    private Integer spiritSelect;//用户景点精灵选择
    private Integer headSelect;//用户头像选择

}
