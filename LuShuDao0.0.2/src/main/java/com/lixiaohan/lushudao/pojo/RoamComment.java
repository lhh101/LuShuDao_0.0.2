package com.lixiaohan.lushudao.pojo;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class RoamComment {
    private Integer commentId;//评论编号(N)
    private Integer userId;//评论用户编号(N)
    private Integer scenicReleaseId;//景点发布信息编号(N)
    private Timestamp commentTime;//评论时间(N)
    private String commentContent;//评论内容(N)
    private Integer commentGood;//点赞数 (N)
}
