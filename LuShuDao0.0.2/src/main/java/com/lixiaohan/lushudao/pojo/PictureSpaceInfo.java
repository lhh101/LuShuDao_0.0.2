package com.lixiaohan.lushudao.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor//只加有参构造
@NoArgsConstructor//补上无参构造
public class PictureSpaceInfo {
   private Integer psId;//图片空间关联表id(N)
   private Integer pictureId;//图片库id(N)
   private Integer spaceId;//说说id(N)
}
