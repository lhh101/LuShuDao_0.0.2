package com.lixiaohan.lushudao.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor//只加有参构造
@NoArgsConstructor//补上无参构造
public class PictureScenicReleaseInfo {
    private Integer psId;  //图片关系id
    private Integer pictureId;  //图片id
    private Integer scenicReleaseId;  //景点发布id
}
