package com.lixiaohan.lushudao.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor//只加有参构造
@NoArgsConstructor//补上无参构造
public class RoamPicture {
    private Integer pictureId;//图片id(N)
    private String pictureUrl;//图片地址(N)
    private Integer userId;//用户id(N)
}
