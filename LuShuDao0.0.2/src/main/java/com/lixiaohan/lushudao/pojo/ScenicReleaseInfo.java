package com.lixiaohan.lushudao.pojo;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class ScenicReleaseInfo {
    private Integer scenicReleaseId;//景点发布信息编号(N)
    private String scenicReleaseTitle;//信息标题(N)
    private String scenicReleaseText;//信息内容(N)
    private Integer scenicReleaseGood;//点赞数
    private Timestamp spaceReleaseTime;//个人空间发布时间(N)
    private Integer spaceReleaseType;//信息类型
    private Integer scenicId;//对应景点编号(N)
    private Integer userId;//发布的用户id(N)
    private Integer anoUser;//是否匿名(N)
}
