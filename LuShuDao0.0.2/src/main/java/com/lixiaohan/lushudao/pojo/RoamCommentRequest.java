package com.lixiaohan.lushudao.pojo;

import lombok.Data;

@Data
public class RoamCommentRequest {
    private String content;
    private Integer srid;
    private Integer uid;
}
