package com.lixiaohan.lushudao.pojo;

import lombok.Data;

@Data
public class RoamScenicCollect {
    private Integer collectId;//收藏品编号(N)
    private String collectName;//收藏品名称(N)
    private Integer pictureState;//收藏品状态(N)
    private Integer userId;//用户id(N)
}
