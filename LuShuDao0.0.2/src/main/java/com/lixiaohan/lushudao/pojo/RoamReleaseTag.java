package com.lixiaohan.lushudao.pojo;

import lombok.Data;

/**
 * tag与景点发布信息关系表
 */
@Data
public class RoamReleaseTag {
    private Integer rtId;//发布信息tag对应表id(N)
    private Integer tagId;//话题编号(N)
    private Integer scenicReleaseId;//景点发布信息编号(N)
}
