package com.lixiaohan.lushudao.pojo;

import lombok.Data;

@Data
public class RoamAdminInfo {
    private Integer adminId;//管理员编号(N)
    private String adminAcc;//管理员账户(N)
    private String adminPwd;//管理员密码(N)
}
