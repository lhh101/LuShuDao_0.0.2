package com.lixiaohan.lushudao.controller;

import com.lixiaohan.lushudao.controller.response.OffBBSResponse;
import com.lixiaohan.lushudao.dao.*;
import com.lixiaohan.lushudao.pojo.*;
import com.lixiaohan.lushudao.util.GetOffBBSResponseListFormSrList;
import com.lixiaohan.lushudao.util.GetSubListFromSrList;
import com.lixiaohan.lushudao.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 景点介绍模块
 */
@RestController
@RequestMapping("/ScenicRelease")
public class ScenicReleaseInfoController {

    /**
     * 根据景点编号sid查询景点发布信息
     * @param sid
     * @return
     */
    @RequestMapping("/findAllScenicReleaseBySid")
    public Result findAllScenicReleaseBySid(Integer sid){
        SqlSession sqlSession=null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            ScenicReleaseInfoMapper mapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            List<ScenicReleaseInfo> scenicReleaseInfoList = mapper.findAllBySid(sid);
            if (scenicReleaseInfoList!=null){
                return Result.ok().message("成功").data("scenicReleaseInfoList",scenicReleaseInfoList);
            }
            return Result.error().message("失败");
        } catch (Exception e) {
            return Result.error().message("服务器出错");
        } finally {
            if (sqlSession!=null)
                sqlSession.close();
        }
    }


    /**
     * @param scenicReleaseInfo 填加一个景点发布信息
     * @param tagTextList 如果没有tag就向tag表中添加
     * @return
     */
    @RequestMapping(value = "/addOneSRI",method = RequestMethod.POST)
    public Result addOneSRI(ScenicReleaseInfo scenicReleaseInfo, String[] tagTextList, HttpSession session){
        SqlSession sqlSession=null;
        try {
            if (scenicReleaseInfo.getAnoUser()==null){
                scenicReleaseInfo.setAnoUser(0);
            }
            RoamUserInfo usersession = (RoamUserInfo) session.getAttribute("user");
            Integer uid=usersession.getUserId();
            scenicReleaseInfo.setUserId(uid);
            sqlSession = MybatisUtils.getSqlSession();
            RoamTagMapper mapperTag = sqlSession.getMapper(RoamTagMapper.class);
            RoamReleaseTagMapper mapperReleaseTag = sqlSession.getMapper(RoamReleaseTagMapper.class);
            ScenicReleaseInfoMapper mapperScenic = sqlSession.getMapper(ScenicReleaseInfoMapper.class);

            Date date=new Date();
            Timestamp timestamp=new Timestamp(date.getTime());
            scenicReleaseInfo.setSpaceReleaseTime(timestamp);
            mapperScenic.addOneSRI(scenicReleaseInfo);//添加景点发布信息
            Integer srid=scenicReleaseInfo.getScenicReleaseId();//取得srid

//            String[] tagTextList={"熊猫","大熊猫"};

            for (String tagText : tagTextList) {//遍历tag数组
                Integer tagid = mapperTag.findTagIdByTtext(tagText);
                if (tagid!=null){//此tag存在
                    //把tagid设置到关系表中
                    mapperReleaseTag.addReleaseTag(tagid,srid);//向关系tag表中添加值
                }else {//tag不存在需要添加tag
                    //添加tag到tag表
                    RoamTag roamTag = new RoamTag();
                    roamTag.setTagText(tagText);
                    mapperTag.addTag(roamTag);
                    Integer tagidReture=roamTag.getTagId();//返回tagId
                    mapperReleaseTag.addReleaseTag(tagidReture,srid);//向关系tag表中添加值
                }
            }
        } catch (Exception e) {
            return Result.error().message("服务器出错");
        } finally {
            if (sqlSession!=null)
            sqlSession.close();
        }
        return Result.ok().message("成功");
    }

    /**
     * 根据景点id-sid和景点类型找到一个景点发布信息
     * @param sid
     * @param type
     * @return
     */
    @RequestMapping("/findAllScenicReleaseBySidAndType")
    public Result findOneScenicReleaseBySidAndType(Integer sid,Integer type){
        List<ScenicReleaseInfo> scenicReleaseInfoList = null;
        SqlSession sqlSession=null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            ScenicReleaseInfoMapper mapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            scenicReleaseInfoList = mapper.findAllBySidAndType(sid,type);
            return Result.ok().message("成功").data("scenicReleaseInfoList",scenicReleaseInfoList);
        } catch (Exception e) {
            return Result.error().message("失败");
        } finally {
            if (sqlSession!=null)
            sqlSession.close();
        }
    }

    /**
     * 根据用户id——uid查询景点发布信息
     * @return
     */
    @RequestMapping("/findAllScneicByUid")
    public Result findAllScneicByUid( @Nullable Integer pagesize, Integer spaceReleaseType){
        SqlSession sqlSession=null;
        try {
//            RoamUserInfo usersession = (RoamUserInfo) session.getAttribute("user");
//            Integer uid=usersession.getUserId();

            Integer uid  = 115;

            System.out.println("XXXXXXXXXXXXXXXXX");
            System.out.println(pagesize);
            System.out.println(spaceReleaseType);

            sqlSession = MybatisUtils.getSqlSession();
            ScenicReleaseInfoMapper mapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            RoamCommentMapper commentMapper = sqlSession.getMapper(RoamCommentMapper.class);
            RoamReleaseTagMapper tagMapper = sqlSession.getMapper(RoamReleaseTagMapper.class);
            RoamTagMapper tagTextMapper = sqlSession.getMapper(RoamTagMapper.class);
            SriGoodInfoMapper goodMapper = sqlSession.getMapper(SriGoodInfoMapper.class);
            RoamUserInfoMapper userMapper = sqlSession.getMapper(RoamUserInfoMapper.class);
            RoamUserInfoMapper mapper1 = sqlSession.getMapper(RoamUserInfoMapper.class);
            RoamUserInfo usersession = mapper1.FindOneUserByUid(uid);

            List<ScenicReleaseInfo> scenicReleaseInfoList;
            if (spaceReleaseType.equals(0)){
                scenicReleaseInfoList = mapper.findALLByUid(uid);
            }else {
                scenicReleaseInfoList = mapper.findAllScenic();
            }


            if (scenicReleaseInfoList!=null){
                ArrayList<Map> list = new ArrayList<>();
                scenicReleaseInfoList.forEach((srInfo)->{
                    String headUrl = "";
                    String userName = "";
                    HashMap<String ,Object> map=new HashMap<>();
                    if (spaceReleaseType.equals(0)){
                          headUrl = usersession.getUserHead();
                          userName = usersession.getUserName();
                    }else {
                        if (Objects.isNull(userMapper.FindOneUserByUid(srInfo.getUserId()))){
                            return;
                        }
                        RoamUserInfo userInfo = userMapper.FindOneUserByUid(srInfo.getUserId());
                        headUrl = userInfo.getUserHead();
                        userName = userInfo.getUserName();
                    }
                    map.put("headUrl",headUrl);
                    map.put("userName",userName);
                    Integer srid = srInfo.getScenicReleaseId();
                    Integer commentCount= commentMapper.findCommentCountBySrid(srid);

                    map.put("commentCount",commentCount);
                    List<Integer> allTagBaySrid = tagMapper.findAllTagBaySrid(srInfo.getScenicReleaseId());
                    allTagBaySrid.forEach(System.out::println);
                    List<String> tags=new ArrayList<>();
                    allTagBaySrid.forEach(tagid->{
                        RoamTag oneTag = tagTextMapper.findOneTagByTagid(tagid);
                        tags.add(oneTag.getTagText());
                    });
                    map.put("tags",tags);
                    String scenicReleaseText = srInfo.getScenicReleaseText();
                    String htmlStr = scenicReleaseText;
                    //<h1>青城 日子</h1><br><img src="http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userSpaceImg/20220322/434e599644974ee3814c65ea6d280d11.jpg" alt="cat" width="390&quot;&quot;">
                    Set<String> pics = new HashSet<>();
                    String img = "";
                    Pattern p_image;
                    Matcher m_image;
                    //     String regEx_img = "<img.*src=(.*?)[^>]*?>"; //图片链接地址
                    String regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>";
                    p_image = Pattern.compile
                            (regEx_img, Pattern.CASE_INSENSITIVE);
                    m_image = p_image.matcher(htmlStr);
                    while (m_image.find()) {
                        // 得到<img />数据
                        img = m_image.group();
                        // 匹配<img>中的src数据
                        Matcher m = Pattern.compile("src\\s*=\\s*\"?(.*?)(\"|>|\\s+)").matcher(img);
                        while (m.find()) {
                            pics.add(m.group(1));
                        }
                    }
                    String[] urls = (String[]) pics.toArray(new String[pics.size()]);
                    List  newUrls = new ArrayList();

                    if(pics.size()>3){
                        newUrls.addAll(Arrays.asList(urls).subList(0, 3));
                    }else {
                        newUrls.addAll(Arrays.asList(urls));
                    }
                    map.put("pictures",newUrls);
                    srInfo.setScenicReleaseText("");
                    map.put("scenicReleaseInfo",srInfo);
                    SriGoodInfo sriGoodInfo = goodMapper.seleStatueByUidAndSrid(srInfo.getScenicReleaseId(), uid);
                    if (!Objects.isNull(sriGoodInfo) && sriGoodInfo.getStatus().equals(1)){
                        map.put("isGood",Boolean.TRUE);
                    }else {
                        map.put("isGood",Boolean.FALSE);
                    }
                    list.add(map);
                });
                List<Map> subList = new ArrayList<>();
                if (list.size()<10){
                    subList.addAll(list);
                } else if (list.size()-10-pagesize<0){
                    subList.addAll(list.subList(0,  list.size()-pagesize));
                } else if (Objects.isNull(pagesize) || pagesize <1){
                    subList.addAll(list.subList(list.size()-10,  list.size()));
                }else {
                    subList.addAll(list.subList(list.size()-10-pagesize,  list.size()-pagesize));
                }

                List<Map> newSubList = subList.stream().sorted(Comparator.comparing(sub -> {
                    ScenicReleaseInfo scenicReleaseInfo = (ScenicReleaseInfo) sub.get("scenicReleaseInfo");
                    return scenicReleaseInfo.getScenicReleaseId();
                }, Comparator.reverseOrder())).collect(Collectors.toList());

                return Result.ok().message("成功").data("AllInfo",newSubList);
            }
            return Result.error().message("失败");
        } catch (Exception e) {
            return Result.error().message("服务器出错").data("error",e);
        } finally {
            if (sqlSession!=null)sqlSession.close();
        }
    }

    /**
     * 用户修改景点发布信息
     *只能修改scenic_release_title,scenic_release_text,
     * space_release_type,scenic_id,ano_user 5个字段，时间会自动更新
     */
    @RequestMapping(value = "/updateOneScenicBySrid",method = RequestMethod.POST)
    public Result updateOneScenicBySrid(ScenicReleaseInfo scenicReleaseInfo,HttpSession session){
        SqlSession sqlSession=null;
        try {
            RoamUserInfo usersession = (RoamUserInfo) session.getAttribute("user");
            Integer uid=usersession.getUserId();
            scenicReleaseInfo.setUserId(uid);
            sqlSession = MybatisUtils.getSqlSession();
            ScenicReleaseInfoMapper mapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            Date date = new Date();
            Timestamp timestamp=new Timestamp(date.getTime());
            scenicReleaseInfo.setSpaceReleaseTime(timestamp);
            Integer integer = mapper.updateScenicReleaseBySrid(scenicReleaseInfo);
            if (integer==1){
                return Result.ok().message("成功");
            }else {
                return Result.error().message("失败");
            }
        } catch (Exception e) {
            return Result.error().message("服务器出错了");
        } finally {
            if (sqlSession!=null)
                sqlSession.close();
        }
    }

    /**
     * 删除一个景点发布信息
     */
    @RequestMapping(value = "/deleOneScenicBySrid")
    public Result deleOneScenicBySrid(Integer srid,HttpSession session){
        SqlSession sqlSession=null;
        try {
            RoamUserInfo usersession = (RoamUserInfo) session.getAttribute("user");
            Integer uid=usersession.getUserId();
            sqlSession = MybatisUtils.getSqlSession();
            ScenicReleaseInfoMapper mapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            Integer flag = mapper.deleScenicReleaseBySrid(srid,uid);
            if (flag==1){
                return Result.ok().message("成功");
            }else {
                return Result.error().message("失败");
            }
        } catch (Exception e) {
            return Result.error().message("服务器出错");
        } finally {
            if (sqlSession!=null)
                sqlSession.close();
        }
    }

    /**
     * 用户为景点发布信息点赞（此处会用到景点发布信息点赞表）
     */
    @RequestMapping("/giveOrDeleGood")
    public Result giveOrDeleGood(Integer srid,HttpSession session){
        SqlSession sqlSession=null;
        try {
            RoamUserInfo usersession = (RoamUserInfo) session.getAttribute("user");
            Integer uid=usersession.getUserId();
            System.out.println(uid+srid);
            sqlSession = MybatisUtils.getSqlSession();
            SriGoodInfoMapper mapperGood = sqlSession.getMapper(SriGoodInfoMapper.class);
            ScenicReleaseInfoMapper mapperScenic = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            ScenicReleaseInfo oneBySrid = mapperScenic.findOneBySrid(srid);
            Integer oldGood = oneBySrid.getScenicReleaseGood();//获取原来的点赞数
            //检查是否点赞过
            SriGoodInfo sriGoodInfoHave = mapperGood.seleStatueByUidAndSrid(srid, uid);
            if (sriGoodInfoHave!=null){//点赞过，执行删除点赞
                Integer integer = mapperGood.deleSriGood(srid, uid);
                if (integer==1){
                    //删除成功
                    //点赞减一
                    Integer flag = mapperScenic.updateGoodNumber(srid, oldGood - 1);
                    if (flag==1){
                        return Result.ok().message("成功");
                    }else {
                        return Result.error().message("失败");
                    }
                }else {
                    //删除失败
                    return Result.error().message("失败");
                }
            }else {//未点赞，执行添加点赞
                SriGoodInfo sriGoodInfo = new SriGoodInfo();
                Date date = new Date();
                Timestamp timestamp=new Timestamp(date.getTime());
                sriGoodInfo.setCreateTime(timestamp);
                sriGoodInfo.setUserId(uid);
                sriGoodInfo.setStatus(1);
                sriGoodInfo.setScenicReleaseId(srid);
                Integer integer = mapperGood.addSriGood(sriGoodInfo);
                if (integer==1){
                    //点赞记录添加成功
                    //点赞加一
                    Integer flag = mapperScenic.updateGoodNumber(srid, oldGood + 1);
                    if (flag==1){
                        return Result.ok().message("成功");
                    }else {
                        return Result.error().message("失败");
                    }
                }else {
                    //添加失败
                    return Result.error().message("失败");
                }
            }
        } catch (Exception e) {
            return Result.error().message("服务器出错");
        } finally {
            if (sqlSession!=null)
                sqlSession.close();
        }

    }

    /**
     * 根据话题内容，查询景点发布信息
     * @param tagText
     * @return
     */
    @RequestMapping("/findAllByTagText")
    public Result findAllByTagText(String tagText,Integer pageSize){
        SqlSession sqlSession = null;
        List<ScenicReleaseInfo> scenicReleaseInfoList = null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            RoamTagMapper mapperTag = sqlSession.getMapper(RoamTagMapper.class);
            Integer tagId = mapperTag.findTagIdByTtext(tagText);//获得tagid
            RoamReleaseTagMapper mapperReleaseTag = sqlSession.getMapper(RoamReleaseTagMapper.class);
            ScenicReleaseInfoMapper mapperScenic = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            List<Integer> sridList = mapperReleaseTag.findAllSridByTagid(tagId);//获得srid集合
            scenicReleaseInfoList = new ArrayList<>();
            for (Integer srid : sridList) {
                ScenicReleaseInfo scenicReleaseInfo = mapperScenic.findOneBySrid(srid);
                if (scenicReleaseInfo!=null){
                    scenicReleaseInfoList.add(scenicReleaseInfo);
                }
            }
            List<ScenicReleaseInfo> reScenicReleaseInfoList = scenicReleaseInfoList.stream().sorted((o1, o2) -> {
                if (o1.getScenicReleaseId() < o2.getScenicReleaseId()) {
                    return 1;
                } else if (o1.getScenicReleaseId().equals(o2.getScenicReleaseId())) {
                    return 0;
                }
                return -1;
            }).collect(Collectors.toList());
            if (pageSize==null || pageSize<0) {pageSize=0;}
            List<ScenicReleaseInfo> pageList;
            int listSize = reScenicReleaseInfoList.size();
            if (listSize<pageSize){
                pageList=null;
            }else if (listSize<pageSize+10){
                pageList=reScenicReleaseInfoList.subList(pageSize,listSize);
            }else{
                pageList= reScenicReleaseInfoList.subList(pageSize, pageSize + 10);
            }
            List<Map> subList = GetSubListFromSrList.get(pageList);
            return Result.ok().message("成功").data("scenicWithPageList",subList);
        } catch (Exception e) {
            return Result.error().message("服务器错误").data("e",e);
        } finally {
            if (sqlSession!=null){
                sqlSession.close();
            }
        }

    }

    /**
     * 为景点发布信息添加评论（前端传来三个参数，时间自动生成，点赞数默认为0）
     * @param
     * @return
     */
    @RequestMapping(value = "/addRoamComment",method = RequestMethod.POST)
    public Result addRoamComment(String content,HttpSession session){
        SqlSession sqlSession=null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            Integer srid = (Integer) session.getAttribute("srid");
             RoamUserInfo userInfo = (RoamUserInfo) session.getAttribute("user");
             Integer uid = userInfo.getUserId();
            RoamUserInfoMapper userInfoMapper = sqlSession.getMapper(RoamUserInfoMapper.class);
            RoamCommentMapper commentMapper = sqlSession.getMapper(RoamCommentMapper.class);
            RoamComment roamComment = new RoamComment();
            roamComment.setUserId(uid);
            Date date = new Date();
            Timestamp timestamp=new Timestamp(date.getTime());
            roamComment.setCommentTime(timestamp);
            roamComment.setCommentContent(content);
            roamComment.setCommentGood(0);
            roamComment.setScenicReleaseId(srid);
            sqlSession = MybatisUtils.getSqlSession();
            RoamCommentMapper mapper = sqlSession.getMapper(RoamCommentMapper.class);
            Integer integer = mapper.addOneComment(roamComment);
            if (integer==1){
                List<RoamComment> commentList = commentMapper.findAllCommentBySrid(srid);
                List<Map<String, Object>> commentDetailList = commentList.stream()
                        .sorted(Comparator.comparing(RoamComment::getCommentTime).reversed())
                        .map(commentItem -> {
                        Map<String, Object> commentDetailMap = new HashMap<>();
                        Integer commentUserId = commentItem.getUserId();
                        RoamUserInfo commentUserInfo = userInfoMapper.FindOneUserByUid(commentUserId);
                        commentDetailMap.put("commentInfo", commentItem);
                        commentDetailMap.put("commentUserInfo", commentUserInfo);
                    return commentDetailMap;
                }).collect(Collectors.toList());
                return Result.ok().data("commentDetailList",commentDetailList);
            }
            return Result.error();
        }catch (Exception e){
            return Result.error().message("服务器错误");
        }
        finally {
            if (sqlSession!=null){
                sqlSession.close();
            }
        }

    }


    /**
     * 为景点发布信息添加评论（前端传来三个参数，时间自动生成，点赞数默认为0）
     * @param
     * @return
     */
    @RequestMapping(value = "/findRoamComment",method = RequestMethod.GET)
    public Result findRoamComment(HttpSession session){
        SqlSession sqlSession=null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            Integer srid = (Integer) session.getAttribute("srid");
            System.out.println("srid:"+srid);
            RoamUserInfoMapper userInfoMapper = sqlSession.getMapper(RoamUserInfoMapper.class);
            RoamCommentMapper commentMapper = sqlSession.getMapper(RoamCommentMapper.class);
            sqlSession = MybatisUtils.getSqlSession();
            List<RoamComment> commentList = commentMapper.findAllCommentBySrid(srid);
            List<Map<String, Object>> commentDetailList = commentList.stream()
                    .sorted(Comparator.comparing(RoamComment::getCommentTime).reversed())
                    .map(commentItem -> {
                    Map<String, Object> commentDetailMap = new HashMap<>();
                    Integer commentUserId = commentItem.getUserId();
                    RoamUserInfo commentUserInfo = userInfoMapper.FindOneUserByUid(commentUserId);
                    commentDetailMap.put("commentInfo", commentItem);
                    commentDetailMap.put("commentUserInfo", commentUserInfo);
                    return commentDetailMap;
            }).collect(Collectors.toList());
            System.out.println("commentDetailList");
            commentDetailList.forEach(System.out::println);

            return Result.ok().data("commentDetailList",commentDetailList);
        }catch (Exception e){
            return Result.error().message("服务器错误");
        }
        finally {
            if (sqlSession!=null){
                sqlSession.close();
            }
        }

    }




    @RequestMapping("/findOneScenicReleaseBySrid")
    public Result findOneScenicReleaseBySrid(Integer srid){
        SqlSession sqlSession=null;
        try{
            sqlSession= MybatisUtils.getSqlSession();
            ScenicReleaseInfoMapper ScenicReleaseMapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            ScenicReleaseInfo oneBySrid = ScenicReleaseMapper.findOneBySrid(srid);
            System.out.println(oneBySrid);
            if (oneBySrid==null) {
                return Result.error().message("内容为空");
            }
            return Result.ok().message("成功").data("oneBySrid",oneBySrid);
        }catch (Exception e){
            return  Result.error().message("服务器错误").data("exception",e);
        }finally {
            if (sqlSession!=null)
            sqlSession.close();
        }


    }

    @RequestMapping(value = "/findAllScenicWithPage")
    public Result findAllScenicWithPage(Integer pageSize,Integer spaceReleaseType,HttpSession session){
        SqlSession sqlSession=null;
        try{
            sqlSession= MybatisUtils.getSqlSession();
            RoamUserInfo userInfo = (RoamUserInfo) session.getAttribute("user");
            Integer uid = userInfo.getUserId();
            ScenicReleaseInfoMapper mapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            List<ScenicReleaseInfo> scenicReleaseList = mapper.findAllScenicWithPage( pageSize,spaceReleaseType);
            List<OffBBSResponse> responseList = GetOffBBSResponseListFormSrList.get(scenicReleaseList,uid);
            return Result.ok().data("scenicWithPageList",responseList);
        }catch (Exception e){
            return Result.error().message("服务器错误");
        }finally {
            if (sqlSession!=null){
                sqlSession.close();
            }
        }

    }

    @GetMapping("/findAllScenicReleaseInfoByTitleOrTag")
    public Result findAllScenicReleaseInfoByTitleOrTag(String keyword,Integer pageSize,Integer spaceReleaseType,HttpSession session){
        SqlSession sqlSession = null;
        try {
            RoamUserInfo userInfo = (RoamUserInfo) session.getAttribute("user");
            Integer uid = userInfo.getUserId();
            sqlSession = MybatisUtils.getSqlSession();
            RoamReleaseTagMapper rrtMapper = sqlSession.getMapper(RoamReleaseTagMapper.class);
            RoamTagMapper rtmapper = sqlSession.getMapper(RoamTagMapper.class);
            ScenicReleaseInfoMapper sriMapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            List<RoamTag> tagLikeText = rtmapper.findSomeTagLikeText(keyword);
            List<Integer> tagIdSet = tagLikeText.stream().map(RoamTag::getTagId).collect(Collectors.toList());
            List<Integer> sridInTagList = rrtMapper.findAllSridInList(tagIdSet);
            List<ScenicReleaseInfo> scenicReleaseInfoList = sriMapper.findAllScenicWithTitalOrTag(keyword,sridInTagList,pageSize,spaceReleaseType);
            List<OffBBSResponse> responseList = GetOffBBSResponseListFormSrList.get(scenicReleaseInfoList,uid);
            return Result.ok().data("scenicReleaseInfoList",responseList);
        }catch (Exception e){
            return Result.error().message("服务器错误");
        }finally {
            if (sqlSession!=null)
            sqlSession.close();
        }

    }






}
