//package com.lixiaohan.lushudao.controller;
//
//import com.lixiaohan.lushudao.dao.RoamSpaceCommentMapper;
//import com.lixiaohan.lushudao.pojo.Result;
//import com.lixiaohan.lushudao.pojo.RoamSpaceComment;
//import com.lixiaohan.lushudao.util.MybatisUtils;
//import org.apache.ibatis.session.SqlSession;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
///**
// * 个人空间评论Controller
// */
//@RestController
//@RequestMapping("/SpaceComment")
//public class RoamSpaceCommentController {
//
//    /**
//     * 根据空间id——sid查询所有评论
//     */
//    @RequestMapping("/findAllSpaceCommentBySid")
//    public Result findAllSpaceCommentBySid(Integer sid){
//        List<RoamSpaceComment> roamSpaceCommentList = null;
//        SqlSession sqlSession = null;
//        try {
//            sqlSession = MybatisUtils.getSqlSession();
//            RoamSpaceCommentMapper mapper = sqlSession.getMapper(RoamSpaceCommentMapper.class);
//            roamSpaceCommentList = mapper.findAllSpaceCommentBySid(sid);
//        } catch (Exception e) {
//            return Result.error().message("失败");
//        } finally {
//            if (sqlSession!=null)
//                sqlSession.close();
//        }
//        return Result.ok().message("成功").data("roamSpaceCommentList",roamSpaceCommentList);
//    }
//}
