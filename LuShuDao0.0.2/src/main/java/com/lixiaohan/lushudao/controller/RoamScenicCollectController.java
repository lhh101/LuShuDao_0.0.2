package com.lixiaohan.lushudao.controller;

import com.lixiaohan.lushudao.dao.RoamScenicCollectMapper;
import com.lixiaohan.lushudao.pojo.Result;
import com.lixiaohan.lushudao.pojo.RoamScenicCollect;
import com.lixiaohan.lushudao.pojo.RoamUserInfo;
import com.lixiaohan.lushudao.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 用户收藏Controller
 */
@RestController
@RequestMapping("/ScenicCollect")
public class RoamScenicCollectController {
    /**
     * 根据用户id查询收藏集合
     */
    @RequestMapping("/findAllCollectByUid")
    public Result findAllCollectByUid(HttpSession session){
        SqlSession sqlSession=null;
        try {
            RoamUserInfo user = (RoamUserInfo) session.getAttribute("user");
            Integer uid=user.getUserId();
            sqlSession = MybatisUtils.getSqlSession();
            RoamScenicCollectMapper mapper = sqlSession.getMapper(RoamScenicCollectMapper.class);
            List<RoamScenicCollect> allScenicCollect = mapper.findAllScenicCollect(uid);

            return Result.ok().message("成功").data("allScenicCollect",allScenicCollect);
        } catch (Exception e) {
            return Result.error().message("服务器错误");
        } finally {
            if (sqlSession!=null)
                sqlSession.close();
        }
    }

    /**
     * 根据用户ui和收藏品名称修改收藏品状态(状态只能从0改为1)
     * uid 用户id
     * cname 收藏品名称
     */
    @RequestMapping("/updateCollectState")
    public Result updateCollectState(String cname,HttpSession session){
        SqlSession sqlSession=null;
        try {
            RoamUserInfo user = (RoamUserInfo) session.getAttribute("user");
            Integer uid=user.getUserId();
            System.out.println(uid+cname);
            sqlSession = MybatisUtils.getSqlSession();
            RoamScenicCollectMapper mapper = sqlSession.getMapper(RoamScenicCollectMapper.class);
            Integer flag=0;
            flag = mapper.updateStateByUidAndCname(uid, cname,1);
            if (flag==1){
                return Result.ok().message("成功");
            }
            return Result.error().message("失败");
        } catch (Exception e) {
            return Result.error().message("服务器出错");
        } finally {
            if (sqlSession!=null)
                sqlSession.close();
        }
    }

}
