//package com.lixiaohan.lushudao.controller;
//
//import com.lixiaohan.lushudao.dao.*;
//import com.lixiaohan.lushudao.pojo.*;
//import com.lixiaohan.lushudao.util.MybatisUtils;
//import org.apache.ibatis.session.SqlSession;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpSession;
//import java.sql.Timestamp;
//import java.util.Date;
//import java.util.List;
//
//@RestController
//@RequestMapping("/UserSpace")
//public class UserSpaceController {
//
//    /**
//     * 用户添加一个空间
//     * @param userSpace
//     * @return
//     */
//    @RequestMapping(value = "/addOneSpace",method = RequestMethod.POST)
//    public Result addOneSpance(UserSpace userSpace, HttpSession session,String[] pictureUrls){
//        SqlSession sqlSession=null;
//        try {
//            RoamUserInfo usersession = (RoamUserInfo) session.getAttribute("user");
//            Integer uid = usersession.getUserId();
//            userSpace.setUserId(uid);
//            sqlSession = MybatisUtils.getSqlSession();
//            UserSpaceMapper mapper = sqlSession.getMapper(UserSpaceMapper.class);
//            Date date=new Date();
//            Timestamp timestamp=new Timestamp(date.getTime());
//            userSpace.setSpaceTime(timestamp);
//            if (userSpace.getPubUser()==null){
//                userSpace.setPubUser(0);
//            }
//            //判断
//            if ("".equals(userSpace.getSpaceContent())){
//                return Result.error().message("未输入内容");
//            }
//            Integer integer = mapper.addOneUserSpace(userSpace);
//            //此处可以获取sid
//            Integer sid=Integer.parseInt(userSpace.getSpaceId());
//            addSpacePicture(pictureUrls,sid,session);//存储图片
//            if (integer==1){
//                return Result.ok().message("添加成功").data("userSpace",userSpace);
//            }else {
//                return Result.error().message("添加失败");
//            }
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null){
//                sqlSession.close();
//            }
//        }
//    }
//
//    /**
//     * 用户删除一个空间
//     * @param sid
//     * @return
//     */
//    @RequestMapping("/deleOneSpace")
//    public Result deleOneSpace(Integer sid){
//        SqlSession sqlSession=null;
//        try {
//            if (sid==0){
//                Result.error().message("删除id为0");
//            }
//            sqlSession = MybatisUtils.getSqlSession();
//            UserSpaceMapper mapper = sqlSession.getMapper(UserSpaceMapper.class);
//            Integer integer = mapper.deleOneUserSpace(sid);
//            if (integer==1){
//                return Result.ok().message("删除成功");
//            }else {
//                return Result.error().message("删除失败");
//            }
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null){
//                sqlSession.close();
//            }
//        }
//    }
//
//
//    /**
//     * 根据用户id查询此用户所有发布空间
//     * @return
//     */
//    @RequestMapping("/findAllUserSpaceByUid")
//    public Result findAllUserSpaceByUid(HttpSession session){
//        SqlSession sqlSession=null;
//        try {
//            RoamUserInfo usersession = (RoamUserInfo) session.getAttribute("user");
//            Integer uid = usersession.getUserId();
//            if (uid==0){
//                Result.error().message("用户id为0");
//            }
//            sqlSession = MybatisUtils.getSqlSession();
//            UserSpaceMapper mapper = sqlSession.getMapper(UserSpaceMapper.class);
//            List<UserSpace> userSpaceList = mapper.findAllUserSpaceByUid(uid);
//            if (userSpaceList==null){
//                return Result.ok().message("你还没有发布空间");
//            }
//            return Result.ok().message("成功").data("userSpaceList",userSpaceList);
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null)
//                sqlSession.close();
//        }
//    }
//
//    /**
//     * 根据空间id，修改个人空间信息
//     * @param userSpace
//     * @return
//     */
//    @RequestMapping("/updateOneSpace")
//    public Result updateOneSpace(UserSpace userSpace,HttpSession session){
//        SqlSession sqlSession=null;
//        try {
//            RoamUserInfo usersession = (RoamUserInfo) session.getAttribute("user");
//            Integer uid = usersession.getUserId();
//            userSpace.setUserId(uid);
//            sqlSession = MybatisUtils.getSqlSession();
//            Date date = new Date();
//            Timestamp timestamp=new Timestamp(date.getTime());
//            userSpace.setSpaceTime(timestamp);
//            UserSpaceMapper mapper = sqlSession.getMapper(UserSpaceMapper.class);
//            Integer integer = mapper.updateOneSpace(userSpace);
//            if (integer==1)
//            return Result.ok().message("成功");
//            else return Result.error().message("失败");
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null)
//            sqlSession.close();
//        }
//    }
//
//    @RequestMapping("/giveOrDeleGood")
//    public Result giveOrDeleGood(HttpSession session,Integer sid){
//        SqlSession sqlSession = null;
//        try {
//            RoamUserInfo usersession = (RoamUserInfo) session.getAttribute("user");
//            Integer uid = usersession.getUserId();
//            sqlSession=MybatisUtils.getSqlSession();
//            UsGoodInfoMapper mapperGoodInfo = sqlSession.getMapper(UsGoodInfoMapper.class);
//            UserSpaceMapper mapperSpace = sqlSession.getMapper(UserSpaceMapper.class);
//            UsGoodInfo usGoodInfo= mapperGoodInfo.seleStatueByUidAndSid(sid,uid);//根据sid和uid查询是否有点过赞
//            System.out.println(usGoodInfo);
//            if (usGoodInfo!=null){//曾经点过赞
//                if (usGoodInfo.getStatus()==0){//修改状态
//                    mapperGoodInfo.updateStatus(sid,uid,1);
//                    mapperSpace.goodUp(sid);
//                }else {
//                    mapperGoodInfo.updateStatus(sid,uid,0);
//                    if (mapperSpace.findGoodNumberBySid(sid)>0)
//                    mapperSpace.goodDown(sid);
//                }
//            }else {//没点过赞就添加啊
//                UsGoodInfo usGoodInfo2 = new UsGoodInfo();
//                Date date = new Date();
//                Timestamp timestamp=new Timestamp(date.getTime());
//                usGoodInfo2.setCreateTime(timestamp);
//                usGoodInfo2.setSpaceId(sid);
//                usGoodInfo2.setUserId(uid);
//                usGoodInfo2.setStatus(1);
//                mapperGoodInfo.addUsGood(usGoodInfo2);
//                mapperSpace.goodUp(sid);
//            }
//            return Result.ok().message("成功");
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null)
//            sqlSession.close();
//        }
//
//    }
//
//    /**
//     * 为空间发布添加评论
//     * @return
//     */
//    @RequestMapping(value = "/addSpaceComment",method = RequestMethod.POST)
//    public Result addSpaceComment(RoamSpaceComment roamSpaceComment,HttpSession session){
//        SqlSession sqlSession = null;
//        try {
//            RoamUserInfo usersession = (RoamUserInfo) session.getAttribute("user");
//            Integer uid = usersession.getUserId();
//            roamSpaceComment.setUserId(uid);
//            Date date = new Date();
//            Timestamp timestamp=new Timestamp(date.getTime());
//            roamSpaceComment.setSpaceCommentTime(timestamp);
//            sqlSession = MybatisUtils.getSqlSession();
//            RoamSpaceCommentMapper mapper = sqlSession.getMapper(RoamSpaceCommentMapper.class);
//            mapper.addSpaceComment(roamSpaceComment);
//            return Result.ok().message("成功");
//        } catch (Exception e) {
//            return Result.error().message("失败");
//        } finally {
//            if (sqlSession!=null)
//                sqlSession.close();
//        }
//
//    }
//
//    /**
//     * 用户发布空间添加图片
//     * @return
//     */
//    @RequestMapping("/addSpacePicture")
//    public Result addSpacePicture(String[] pictureUrls,Integer sid,HttpSession session){
//        SqlSession sqlSession=null;
//        try {
//            RoamUserInfo user = (RoamUserInfo) session.getAttribute("user");
//            Integer uid = user.getUserId();
//            sqlSession = MybatisUtils.getSqlSession();
//            RoamPictureMapper mapperRoamPic = sqlSession.getMapper(RoamPictureMapper.class);
//            PictureSpaceInfoMapper mapperSpacePic = sqlSession.getMapper(PictureSpaceInfoMapper.class);
//            int picNum=0;
//            RoamPicture roamPicture = new RoamPicture();
//            roamPicture.setUserId(uid);
//            for (String pictureUrl : pictureUrls) {
//                roamPicture.setPictureUrl(pictureUrl);
//                Integer integer = mapperRoamPic.addOnePicture(roamPicture);//返回id
//                Integer flag = mapperSpacePic.addSpacePicture(new PictureSpaceInfo(null, roamPicture.getPictureId(), sid));
//                if (integer==1&&flag==1) picNum++;
//            }
//            if (pictureUrls.length==picNum){
//                return Result.ok().message("成功");
//            }else {
//                return Result.error().message("失败");
//            }
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null)
//                sqlSession.close();
//        }
//
//    }
//
//
//}
