package com.lixiaohan.lushudao.controller.response;


import com.lixiaohan.lushudao.pojo.ScenicReleaseInfo;
import lombok.Data;

import java.util.List;

@Data
public class OffBBSResponse {
    private ScenicReleaseInfo srInfo ;//景点发布信息
    private List<String> imgs; //图片集合
    private List<String> tagTexts; //tag集合
    private Integer comment;//评论数
    private Boolean isGood;//是否点赞
}
