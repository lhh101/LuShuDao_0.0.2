package com.lixiaohan.lushudao.controller;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;
import java.io.IOException;

@Controller
@RequestMapping("/download")
public class DownAPKController {

    @RequestMapping("/apk")
    public ResponseEntity export() throws IOException {
        HttpHeaders headers = new HttpHeaders();
        File file = new File("/www/server/tomcat/webapps/LuShuDao0.0.2-1.0-SNAPSHOT/ROOT/mobileAPP/app-release.apk");
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", "LuShuDao.apk");
        return new ResponseEntity(FileUtils.readFileToByteArray(file), headers, HttpStatus.CREATED);
    }

}
