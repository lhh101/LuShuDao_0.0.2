package com.lixiaohan.lushudao.controller;

import com.lixiaohan.lushudao.dao.RoamScenicMapper;
import com.lixiaohan.lushudao.pojo.Result;
import com.lixiaohan.lushudao.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/Scenic")
public class RoamScenicController {

    /**
     * 根据景点名称找到景点id
     * @param scname
     * @return
     */
    @RequestMapping("/findSidByScname")
    public Result findSidByScname(String scname){
        SqlSession sqlSession=null;
        try {
             sqlSession= MybatisUtils.getSqlSession();
            RoamScenicMapper mapper = sqlSession.getMapper(RoamScenicMapper.class);
            Integer scid = mapper.findSidByScname(scname);

            return Result.ok().message("成功").data("scid",scid);
        } catch (Exception e) {
            return Result.error().message("失败");
        } finally {
            if (sqlSession!=null)
                sqlSession.close();
        }

    }

}
