//package com.lixiaohan.lushudao.controller;
//
//import com.lixiaohan.lushudao.dao.RoamAdminInfoMapper;
//import com.lixiaohan.lushudao.dao.RoamUserInfoMapper;
//import com.lixiaohan.lushudao.dao.ScenicReleaseInfoMapper;
//import com.lixiaohan.lushudao.dao.UserSpaceMapper;
//import com.lixiaohan.lushudao.pojo.*;
//import com.lixiaohan.lushudao.util.MybatisUtils;
//import org.apache.ibatis.session.SqlSession;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpSession;
//import java.util.List;
//
//@RestController
//@RequestMapping("/Admin")
//public class RoamAdminInfoController {
//
//    /**
//     * 添加管理员
//     * @param roamAdminInfo
//     * @return
//     */
//    @RequestMapping(value = "/addAdmin",method = RequestMethod.POST)
//    public Result addAdmin(RoamAdminInfo roamAdminInfo){
//        SqlSession sqlSession=null;
//        try {
//            if ("".equals(roamAdminInfo.getAdminAcc())||roamAdminInfo.getAdminAcc()==null||"".equals(roamAdminInfo.getAdminPwd())||roamAdminInfo.getAdminPwd()==null){
//                return Result.error().message("用户名或密码为空");
//            }
//            sqlSession = MybatisUtils.getSqlSession();
//            RoamAdminInfoMapper mapper = sqlSession.getMapper(RoamAdminInfoMapper.class);
//            //先查询有没有此用户名
//            RoamAdminInfo roamAdminInfo1 = mapper.seleAdminByAcc(roamAdminInfo.getAdminAcc());
//            if (roamAdminInfo1!=null){
//                return Result.error().message("用户名重复");
//            }
//            System.out.println(roamAdminInfo.getAdminAcc());
//            Integer flag = mapper.addAdmin(roamAdminInfo);
//            if (flag==1){
//                return Result.ok().message("成功").data("roamAdminInfo",roamAdminInfo);
//            }
//            return Result.error().message("失败");
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null)
//            sqlSession.close();
//        }
//
//    }
//
//    /**
//     * 管理员登录
//     * @param adminAcc
//     * @param adminPwd
//     * @return
//     */
//    @RequestMapping(value = "/loginAdmin",method = RequestMethod.POST)
//    public Result loginAdmin(String adminAcc, String adminPwd, HttpSession session){
//        SqlSession sqlSession=null;
//        try {
//            if ("".equals(adminAcc)||"".equals(adminPwd)){
//                return Result.error().message("用户名或密码为空");
//            }
//            sqlSession = MybatisUtils.getSqlSession();
//            RoamAdminInfoMapper mapper = sqlSession.getMapper(RoamAdminInfoMapper.class);
//            RoamAdminInfo roamAdminInfo = mapper.seleAdminByAcc(adminAcc);
//            sqlSession.close();
//            if (roamAdminInfo!=null&&roamAdminInfo.getAdminPwd().equals(adminPwd)){
//                session.setAttribute("admin",roamAdminInfo);
//                return Result.ok().message("成功");
//            }
//            return  Result.error().message("失败");
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null)
//                sqlSession.close();
//        }
//    }
//
//
//    /**
//     * 查询所有用户的发布空间
//     * @return
//     */
//    @RequestMapping("/seleAllSpace")
//    public Result seleAllSpace(){
//        SqlSession sqlSession=null;
//        try {
//            sqlSession = MybatisUtils.getSqlSession();
//            UserSpaceMapper mapper = sqlSession.getMapper(UserSpaceMapper.class);
//            List<UserSpace> allSpace = mapper.findAllSpace();
//            sqlSession.close();
//            return Result.ok().message("成功").data("allSpace",allSpace);
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null)
//                sqlSession.close();
//        }
//    }
//
//    /**
//     * 查询所有用户发布的景点
//     * @return
//     */
//    @RequestMapping("/seleAllScenic")
//    public Result seleAllScenic(){
//        SqlSession sqlSession=null;
//        try {
//            sqlSession = MybatisUtils.getSqlSession();
//            ScenicReleaseInfoMapper mapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
//            List<ScenicReleaseInfo> allScenic = mapper.findAllScenic();
//            sqlSession.close();
//            return Result.ok().message("成功").data("allScenic",allScenic);
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null)
//                sqlSession.close();
//        }
//    }
//
//    /**
//     * 根据空间id删除用户的发布空间
//     * @return
//     */
//    @RequestMapping("/deleSpace")
//    public Result deleSpace(Integer sid){
//        SqlSession sqlSession=null;
//        try {
//            sqlSession = MybatisUtils.getSqlSession();
//            UserSpaceMapper mapper = sqlSession.getMapper(UserSpaceMapper.class);
//            Integer integer = mapper.deleOneUserSpace(sid);
//            if (integer==1){
//                return Result.ok().message("成功");
//            }else {
//                return Result.error().message("失败");
//            }
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null)
//                sqlSession.close();
//        }
//
//    }
//
//    /**
//     * 根据发布景点id删除用户发布的景点
//     * @return
//     */
//    @RequestMapping("/deleScenic")
//    public Result deleScenic(Integer srid){
//        SqlSession sqlSession=null;
//        try {
//            sqlSession = MybatisUtils.getSqlSession();
//            ScenicReleaseInfoMapper mapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
//            Integer integer = mapper.deleOneScenicBySrid(srid);
//            if (integer==1){
//                return Result.ok().message("成功");
//            }else {
//                return Result.error().message("失败");
//            }
//        } catch (Exception e) {
//            return Result.error().message("服务器出错");
//        } finally {
//            if (sqlSession!=null)
//                sqlSession.close();
//        }
//
//    }
//
//    /**
//     * 根据用户Acc查询所有发布空间
//     * @param userAcc
//     * @return
//     */
//    @RequestMapping("/seleOneUserSpace")
//    public Result seleOneUserSpace(String userAcc){
//        SqlSession sqlSession=null;
//        try {
//            System.out.println(userAcc);
//            sqlSession = MybatisUtils.getSqlSession();
//            UserSpaceMapper mapperSpace = sqlSession.getMapper(UserSpaceMapper.class);
//            RoamUserInfoMapper mapperUser = sqlSession.getMapper(RoamUserInfoMapper.class);
//            Integer uid=mapperUser.FindUidByUserAcc(userAcc);
//            List<UserSpace> oneUserAllSpace = mapperSpace.findAllUserSpaceByUid(uid);
//            return Result.ok().message("成功").data("oneUserAllSpace",oneUserAllSpace);
//        } catch (Exception e) {
//            return Result.error().message("服务器错误");
//        } finally {
//            if(sqlSession!=null)
//            sqlSession.close();
//        }
//
//    }
//
//    /**
//     * 根据用户Acc查询所有发布景点
//     * @param userAcc
//     * @return
//     */
//    @RequestMapping("/seleOneUserScenic")
//    public Result seleOneUserScenic(String userAcc){
//        SqlSession sqlSession=null;
//        try {
//            sqlSession = MybatisUtils.getSqlSession();
//            RoamUserInfoMapper mapperUser = sqlSession.getMapper(RoamUserInfoMapper.class);
//            Integer uid=mapperUser.FindUidByUserAcc(userAcc);
//            ScenicReleaseInfoMapper mapperScenic = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
//            List<ScenicReleaseInfo> oneUserAllScenic = mapperScenic.findALLByUid(uid);
//            return Result.ok().message("成功").data("oneUserAllScenic",oneUserAllScenic);
//        } catch (Exception e) {
//            return Result.error().message("服务器错误");
//        } finally {
//            if(sqlSession!=null)
//                sqlSession.close();
//        }
//    }
//
//
//
//}
