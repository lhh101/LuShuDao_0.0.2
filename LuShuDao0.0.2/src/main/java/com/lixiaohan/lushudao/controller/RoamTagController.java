package com.lixiaohan.lushudao.controller;

import com.lixiaohan.lushudao.dao.RoamTagMapper;
import com.lixiaohan.lushudao.pojo.Result;
import com.lixiaohan.lushudao.pojo.RoamTag;
import com.lixiaohan.lushudao.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/Tag")
@RestController
public class RoamTagController {
    @RequestMapping("/findAllTag")
    public Result findAllTag(){
        SqlSession sqlSession;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            RoamTagMapper tagMapper = sqlSession.getMapper(RoamTagMapper.class);
            RoamTag[] allTag = tagMapper.findAllTag();
            for (RoamTag tag : allTag) {
                System.out.println(tag);
            }
            return Result.ok().message("成功").data("allTag",allTag);
        }catch (Exception e){
            return Result.error().message("失败").data("Exception",e);
        }

    }
}
