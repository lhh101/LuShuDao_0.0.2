package com.lixiaohan.lushudao.controller;

import com.lixiaohan.lushudao.dao.RoamUserInfoMapper;
import com.lixiaohan.lushudao.pojo.Result;
import com.lixiaohan.lushudao.pojo.RoamUserInfo;
import com.lixiaohan.lushudao.request.PasswordRequest;
import com.lixiaohan.lushudao.service.RoamUserInfoService;
import com.lixiaohan.lushudao.service.imlp.RoamUserInfoServiceImpl;
import com.lixiaohan.lushudao.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * 用户模块
 */
@RestController
@RequestMapping("/UserInfo")
public class RoamUserInfoController {

    @Resource
    private RoamUserInfoService roamUserInfoService;

    /**
     * 测试：查询所有用户
     * @return
     */
    @RequestMapping("/findAllUser")
    public Result findAllUser(){
        SqlSession sqlSession=null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            RoamUserInfoMapper mapper = sqlSession.getMapper(RoamUserInfoMapper.class);
            List<RoamUserInfo> roamUserInfoList= mapper.FindAllUser();
            return Result.ok().message("成功").data("roamUserInfoList",roamUserInfoList);
        } catch (Exception e) {
            return Result.error().message("服务器错误");
        } finally {
            if (sqlSession!=null)
                sqlSession.close();
        }
    }

    /**
     *注册用户
     * 自动填写时间
     */
    @RequestMapping(value = "/addUser",method = RequestMethod.POST)
    public Result AddUser(RoamUserInfo userInfo){
        if ("".equals(userInfo.getUserAcc())||"".equals(userInfo.getUserPwd())){
            return Result.error().message("验证码或密码为空");
        }
        SqlSession sqlSession=null;
        try {
            sqlSession = MybatisUtils.getSqlSession();
            RoamUserInfoMapper mapper = sqlSession.getMapper(RoamUserInfoMapper.class);
            Date date=new Date();
            Timestamp timestamp= new Timestamp(date.getTime());
            userInfo.setUserFirstTime(timestamp);
            mapper.AddOneUser(userInfo);
            return Result.ok().message("成功").data("addUser",userInfo);
        } catch (Exception e) {
            return Result.error().message("服务器错误");
        } finally {
            if (sqlSession!=null)
                sqlSession.close();
        }
    }

    /**
     * 登录验证
     */
    @RequestMapping(value = "/loginUser",method = RequestMethod.POST)
    public Result   LoginUser(String  userAcc, String userPwd, HttpSession session){
        if(!userAcc.equals("")){
            RoamUserInfoService roamUserInfoService=new RoamUserInfoServiceImpl();
            RoamUserInfo  roamUserInfo=roamUserInfoService.LoginUser(userAcc);
            if (roamUserInfo==null){
                return Result.error().message("没有该用户");
            }
            if (!"".equals(userPwd)&&userPwd.equals(roamUserInfo.getUserPwd())){
                session.setAttribute("user",roamUserInfo);
                return Result.ok().message("成功").data("loginUser",roamUserInfo);
            }else {
                return Result.error().message("密码错误");
            }
        }else {
            return Result.error().message("用户名为空");
        }
    }

    /**
     * 修改用户信息
     * 此处只能修改
     * 昵称，虚拟人物图片地址，景点精灵图片地址，用户头像地址，
     * 用户虚拟人物选择，用户景点精灵选择，用户头像选择。
     */
    @RequestMapping(value = "/updateUser",method = RequestMethod.POST)
    public Result UpdateUser(RoamUserInfo userInfo,HttpSession session){
        RoamUserInfo userInfonew= null;
        try {
            RoamUserInfoService userInfoService=new RoamUserInfoServiceImpl();
            RoamUserInfo userInfoSession = (RoamUserInfo) session.getAttribute("user");
            userInfonew = userInfoService.UpdateUserOther(userInfoSession,userInfo);
            if (userInfonew==null){
                return Result.error().message("服务器出错");
            }
            session.removeAttribute("user");
            session.setAttribute("user",userInfonew);
            return Result.ok().message("成功").data("loginUser",userInfonew);
        } catch (Exception e) {
            return Result.error().message("服务器出错");
        }
    }

    @RequestMapping(value = "/logout")
    public Result logoutUser(String email){
        try {
            Integer flag = roamUserInfoService.LogoutUser(email);
            if (flag!=null && flag ==1) return Result.ok().message("用户已删除");
            return Result.error().message("没有此用户或用户已删除");
        }catch (Exception e){
            return Result.error().message("服务器出错");
        }
    }

    @RequestMapping(value = "/updatePassword",method = RequestMethod.POST)
    public Result updatePassword( PasswordRequest request ){
        try {
            roamUserInfoService.UpdateUserPassword(request.getUserAcc(), request.getPassword());
            return Result.ok().message("修改成功");
        }catch (Exception e){
            return Result.error().message(e.getMessage());
        }
    }

}
