package com.lixiaohan.lushudao.controller;

import com.lixiaohan.lushudao.dao.RoamUserInfoMapper;
import com.lixiaohan.lushudao.dao.ScenicReleaseInfoMapper;
import com.lixiaohan.lushudao.pojo.RoamUserInfo;
import com.lixiaohan.lushudao.pojo.ScenicReleaseInfo;
import com.lixiaohan.lushudao.util.MybatisUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.sql.Timestamp;

@Controller
@RequestMapping("/getView")
public class GetViewController {

    @RequestMapping("/getScenicReleaseView/{srid}")
    public String getScenicReleaseView(HttpSession session,@PathVariable Integer srid, Integer uid){
        SqlSession sqlSession=null;
        try {
            System.out.println("渲染视图");
            sqlSession = MybatisUtils.getSqlSession();
            ScenicReleaseInfoMapper srMapper = sqlSession.getMapper(ScenicReleaseInfoMapper.class);
            ScenicReleaseInfo scenicReleaseInfo = srMapper.findOneBySrid(srid);
            if (scenicReleaseInfo!=null){
                String srTitle = scenicReleaseInfo.getScenicReleaseTitle();//景点标题
                String srText = scenicReleaseInfo.getScenicReleaseText();//景点信息内容
                Timestamp spaceReleaseTime = scenicReleaseInfo.getSpaceReleaseTime();
                String srTime = spaceReleaseTime.toString();//景点发布时间
                String time = srTime.substring(0,srTime.length()-2);
                Integer userId = scenicReleaseInfo.getUserId();
                RoamUserInfoMapper userInfoMapper = sqlSession.getMapper(RoamUserInfoMapper.class);
                RoamUserInfo userInfo = userInfoMapper.FindOneUserByUid(userId);
                String headUrl = userInfo.getUserHead();
                System.out.println(uid);
                session.setAttribute("uid",uid);
                session.setAttribute("srid",srid);
                session.setAttribute("title",srTitle);
                session.setAttribute("text",srText);
                session.setAttribute("time",time);
                session.setAttribute("headUrl",headUrl);
                session.setAttribute("userName",userInfo.getUserName());
            }
            return "scenicReleaseView";
        }catch (Exception e){
            session.setAttribute("Exception",e);
            return "error";
        }finally {
            if (sqlSession!=null){sqlSession.close();}
        }

    }





}
