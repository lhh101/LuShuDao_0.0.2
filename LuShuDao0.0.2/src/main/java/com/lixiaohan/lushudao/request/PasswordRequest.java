package com.lixiaohan.lushudao.request;

import lombok.Data;

@Data
public class PasswordRequest {
    private String userAcc;

    private String password;
}
