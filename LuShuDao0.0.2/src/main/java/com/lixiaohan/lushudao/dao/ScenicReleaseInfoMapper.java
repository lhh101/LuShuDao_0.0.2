package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.ScenicReleaseInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ScenicReleaseInfoMapper {

    /**
     * 根据景点发布信息id——srid查找景点发布信息
     */

    ScenicReleaseInfo findOneBySrid(@Param("srid") Integer srid);

    /**
     * 根据景点id找到所有景点发布信息
     * @param sid
     * @return
     */
    List<ScenicReleaseInfo> findAllBySid(@Param("sid") Integer sid);

    /**
     * 添加一个景点发布信息
     */
    Integer addOneSRI(ScenicReleaseInfo scenicReleaseInfo);

    /**
     * 根据景点id-sid和景点类型找到多个景点发布信息
     */
    List<ScenicReleaseInfo> findAllBySidAndType(@Param("sid") Integer sid,@Param("type") Integer type);

    /**
     * 根据用户uid找到所有景点发布信息
     */
    List<ScenicReleaseInfo> findALLByUid(@Param("uid") Integer uid);

    /**
     * 根据景点发布信息的id——srid修改景点发布信息
     */
    Integer updateScenicReleaseBySrid(ScenicReleaseInfo scenicReleaseInfo);

    /**
     * 删除一个景点发布信息
     */
    Integer deleScenicReleaseBySrid(@Param("srid") Integer srid,@Param("uid") Integer uid);

    /**
     * 根据srid修改点赞数
     *
     */
    Integer updateGoodNumber(@Param("srid") Integer srid,@Param("goodNew") Integer goodNew);

    /**
     * 找到所有发布景点
     */
    List<ScenicReleaseInfo> findAllScenic();

    /**
     * 删除一个景点发布信息2（只根据srid）
     */
    Integer deleOneScenicBySrid(@Param("srid") Integer srid);

    /**
     * 分页查询所有景点发布信息
     * @param pageSize
     * @param spaceReleaseType
     * @return
     */
    List<ScenicReleaseInfo> findAllScenicWithPage(@Param("pageSize") Integer pageSize,@Param("spaceReleaseType") Integer spaceReleaseType);

    /**
     * 根据tag 分页查询所有景点发布信息
     * @param keyword
     * @param sridWithTagList
     * @param pageSize
     * @param spaceReleaseType
     * @return
     */
    List<ScenicReleaseInfo> findAllScenicWithTitalOrTag(@Param("keyword") String keyword,@Param("sridWithTagList") List<Integer> sridWithTagList,@Param("pageSize") Integer pageSize,@Param("spaceReleaseType") Integer spaceReleaseType);
}
