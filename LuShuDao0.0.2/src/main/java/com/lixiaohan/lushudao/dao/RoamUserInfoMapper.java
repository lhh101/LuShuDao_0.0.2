package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.RoamUserInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户信息表Dao
 */
public interface RoamUserInfoMapper {
    /**
     * 增加一个用户
     */
    void AddOneUser(RoamUserInfo roamUserInfo);
    /**
     * 修改
     */
    Integer UpdateOneUser(RoamUserInfo roamUserInfo);

    /**
     * 登录根据用户邮箱查询密码是否正确
     */
    RoamUserInfo LoginUser(@Param("userAcc") String  userAcc);
    /**
     * 测试：
     * 查询所有用户
     */
    List<RoamUserInfo> FindAllUser();
    /**
     * 根据uid查询一个用户
     */
    RoamUserInfo FindOneUserByUid(@Param("uid") Integer uid);

    /**
     * 根据用户名查询uid
     */
    Integer FindUidByUserAcc(@Param("userAcc") String userAcc);


    /**
     * 判断邮箱是否被删除，没有删除则删除
     */
    Integer LogoutByUid(@Param("uid") Integer uid);

    /**
     * 修改密码
     */
    Integer updatePassword(@Param("userAcc") String userAcc,@Param("password") String password);

}
