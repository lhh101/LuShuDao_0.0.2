package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.RoamComment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 评论Dao
 */
public interface RoamCommentMapper {
    /**
     * 根据景点发布srid查询所有评论
     */
    List<RoamComment> findAllCommentBySrid(@Param("srid") Integer srid);

    /**
     * 添加景点信息评论
     * @param roamComment
     * @return
     */
    Integer addOneComment(RoamComment roamComment);

    /**
     * 根据用户id查询所有评论
     */
    List<RoamComment> findAllCommentByUid(@Param("uid") Integer uid);

    /**
     * 根据景点发布srid查询所有评论
     * @param srid
     * @return
     */
    Integer findCommentCountBySrid(@Param("srid") Integer srid);



}
