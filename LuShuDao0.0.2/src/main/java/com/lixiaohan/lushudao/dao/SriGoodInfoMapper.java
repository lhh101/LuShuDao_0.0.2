package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.SriGoodInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 景点发布信息点赞Dao
 */
public interface SriGoodInfoMapper {

    /**
     * 添加(四个字段)
     * @return
     */
    Integer addSriGood(SriGoodInfo sriGoodInfo);

    /**
     * 根据参数删除
     * @param srid
     * @param uid
     * @return
     */
    Integer deleSriGood(@Param("srid") Integer srid, @Param("uid") Integer uid);

    /**
     * 根据uid和srid查询状态（检查是否点赞过）
     */
    SriGoodInfo seleStatueByUidAndSrid(@Param("srid") Integer srid, @Param("uid") Integer uid);

}
