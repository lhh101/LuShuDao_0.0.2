package com.lixiaohan.lushudao.dao;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * tag与景点发布信息关系Dao
 */
@Component
public interface RoamReleaseTagMapper {

    /**
     * 添加景点信息与tag的关系
     * @param tagId
     * @param srid
     * @return
     */
    Integer addReleaseTag(@Param("tagId") Integer tagId,@Param("srid") Integer srid);

    /**
     * 根据tagid查询srid集合
     */
    List<Integer> findAllSridByTagid(@Param("tagId") Integer tagId);

    /**
     * 根据景点srid找含有的tag
     */
    List<Integer> findAllTagBaySrid(@Param("srid") Integer srid);

    /**
     * tagid包含在集合中的所有srid
     */
    List<Integer> findAllSridInList(List<Integer> list);

}
