package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.RoamAdminInfo;
import org.apache.ibatis.annotations.Param;

public interface RoamAdminInfoMapper {
    //添加管理员
    Integer addAdmin(RoamAdminInfo adminInfo);

    //根据管理员账号查密码
    RoamAdminInfo seleAdminByAcc(@Param("adminAcc") String adminAcc);


}
