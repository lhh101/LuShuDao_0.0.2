package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.RoamScenicCollect;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户收藏Dao
 */
public interface RoamScenicCollectMapper {
    /**
     * 查询用户收藏品
     * 此处有疑问，是一个收藏品还是一个集合
     */
    List<RoamScenicCollect> findAllScenicCollect(@Param("uid") Integer uid);

    /**
     * 根据uid和收藏品名称修改状态
     * @param uid
     * @return
     */
    Integer updateStateByUidAndCname(@Param("uid") Integer uid,@Param("cname") String cname,@Param("state") Integer state);

    /**
     * 根据uid和cname找到一个收藏品
     * @param uid
     * @param cname
     * @return
     */
    RoamScenicCollect findScenicCollectByUidAndCname(@Param("uid") Integer uid,@Param("cname") String cname);
}
