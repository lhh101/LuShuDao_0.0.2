package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.PictureSpaceInfo;

/**
 * 用户空间图片关系模块
 */
public interface PictureSpaceInfoMapper {

    /**
     * 添加空间图片关系
     * @return
     */
    public Integer addSpacePicture(PictureSpaceInfo pictureSpaceInfo);
}
