package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.RoamPicture;

/**
 * 图片模块
 */
public interface RoamPictureMapper {

   /**
    * 添加图片
    * @return
    */
   public Integer addOnePicture(RoamPicture roamPicture);

   /**
    * 删除图片
    * @param picUrl
    * @return
    */
   public Integer deleOnePicture(String picUrl);

   /**
    * 根据图片id找到一个图片信息
    * @param pid
    * @return
    */
   RoamPicture findOneByPid(Integer pid);
}
