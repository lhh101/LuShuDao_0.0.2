package com.lixiaohan.lushudao.dao;

import org.apache.ibatis.annotations.Param;

/**
 * 景点发布图片关系模块
 */
public interface PictureScenicReleaseInfoMapper {

    /**
     * 添加一个景点发布图片关系
     * @param pid
     * @param srid
     * @return
     */
     Integer addOneScenicRela(@Param("pid") Integer pid, @Param("srid") Integer srid);

    /**
     * 根据srid找到对应的图片id
     * @param srid
     * @return
     */
    Integer findOneBySrid(@Param("srid") Integer srid);

}
