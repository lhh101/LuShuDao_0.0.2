package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.RoamSpaceComment;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoamSpaceCommentMapper {

    /**
     * 为用户发布空间添加评论(四个字段)space_comment_time,space_comment_content,space_id,user_id
     */
    Integer addSpaceComment(RoamSpaceComment roamSpaceComment);

    /**
     * 根据空间id——sid查询评论列表
     */
    List<RoamSpaceComment> findAllSpaceCommentBySid(@Param("sid") Integer sid);
}
