package com.lixiaohan.lushudao.dao;

import org.apache.ibatis.annotations.Param;

public interface RoamScenicMapper {
    /**
     * 根据景点名称找到景点id
     */
    Integer findSidByScname(@Param("scname") String scname);

}
