package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.UsGoodInfo;
import org.apache.ibatis.annotations.Param;

public interface UsGoodInfoMapper {
    /**
     * 添加(四个字段)user_id,create_time,space_id,status
     * @return
     */
    Integer addUsGood(UsGoodInfo usGoodInfo);

//    /**
//     * 根据参数删除
//     * @param sid
//     * @param uid
//     * @return
//     */
//    Integer deleUsGood(@Param("sid") Integer sid, @Param("uid") Integer uid);//无

    /**
     * 根据uid和sid查询状态（检查是否点赞过）
     */
    UsGoodInfo seleStatueByUidAndSid(@Param("sid") Integer sid, @Param("uid") Integer uid);

    /**
     * 更改点赞状态
     */
    Integer updateStatus(@Param("sid") Integer sid, @Param("uid") Integer uid,@Param("status") Integer status);

}
