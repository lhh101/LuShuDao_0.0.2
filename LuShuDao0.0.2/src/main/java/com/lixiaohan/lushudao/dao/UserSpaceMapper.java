package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.UserSpace;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserSpaceMapper {
    /**
     * 添加用户空间
     * @param userSpace
     * @return
     */
    Integer addOneUserSpace(UserSpace userSpace);

    /**
     * 删除一个用户空间
     * @param sid
     * @return
     */
    Integer deleOneUserSpace(@Param("sid") Integer sid);

    /**
     * 根据用户id找到多个用户空间
     * @param uid
     * @return
     */
    List<UserSpace> findAllUserSpaceByUid(@Param("uid") Integer uid);

    /**
     * 修改一个用户空间（传入三个参数spaceId，spaceContent,pubUser）
     * @param userSpace
     * @return
     */
    Integer updateOneSpace(UserSpace userSpace);

    /**
     * 根据sid查询点赞数
     * @param sid
     * @return
     */
    Integer findGoodNumberBySid(@Param("sid") Integer sid);

    /**
     * 点赞数自增
     * @return
     */
    Integer goodDown(@Param("sid") Integer sid);

    /**
     * 点赞数自减
     * @param sid
     * @return
     */
    Integer goodUp(@Param("sid") Integer sid);

    /**
     * 查询所有用户空间
     */
    List<UserSpace> findAllSpace();



}
