package com.lixiaohan.lushudao.dao;

import com.lixiaohan.lushudao.pojo.RoamTag;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RoamTagMapper {
    /**
     * 通过tag内容找到tag的id
     * @param tagText
     * @return
     */
    Integer findTagIdByTtext(@Param("tagText") String tagText);//没为它写独立控制器

    /**
     * 添加一个tag
     * @return
     */
    Integer addTag(RoamTag roamTag);

    /**
     * 查找所有的Tag
     */
    RoamTag[] findAllTag();

    /**
     * 通过tagid找tag
     */
    RoamTag findOneTagByTagid(@Param("tid") Integer tid);


    List<RoamTag> findSomeTagLikeText(@Param("text") String text);

}
