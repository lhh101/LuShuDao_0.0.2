package com.lixiaohan.lushudao.service;

import com.lixiaohan.lushudao.pojo.RoamUserInfo;

public interface RoamUserInfoService {
    /**
     * 登录验证
     */
    RoamUserInfo LoginUser(String userAcc);

    /**
     * 修改用户部分信息
     */
    RoamUserInfo UpdateUserOther(RoamUserInfo userInfoSession,RoamUserInfo userInfo);

    /**
     * 查询根据邮箱查询是否有这一个人，有则删除
     */
    Integer LogoutUser(String email);

    /**
     * 修改密码
     * @param userAcc
     * @param password
     * @return
     */
    Integer UpdateUserPassword(String userAcc,String password);

}
