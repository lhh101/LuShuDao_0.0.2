package com.lixiaohan.lushudao.util;

import com.lixiaohan.lushudao.dao.RoamCommentMapper;
import com.lixiaohan.lushudao.dao.RoamReleaseTagMapper;
import com.lixiaohan.lushudao.dao.RoamTagMapper;
import com.lixiaohan.lushudao.dao.RoamUserInfoMapper;
import com.lixiaohan.lushudao.pojo.RoamTag;
import com.lixiaohan.lushudao.pojo.RoamUserInfo;
import com.lixiaohan.lushudao.pojo.ScenicReleaseInfo;
import org.apache.ibatis.session.SqlSession;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetSubListFromSrList {

    public static List<Map> get(List<ScenicReleaseInfo> scenicReleaseInfoList){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        RoamUserInfoMapper userMapper = sqlSession.getMapper(RoamUserInfoMapper.class);
        RoamCommentMapper commentMapper = sqlSession.getMapper(RoamCommentMapper.class);
        RoamReleaseTagMapper tagMapper = sqlSession.getMapper(RoamReleaseTagMapper.class);
        RoamTagMapper tagTextMapper = sqlSession.getMapper(RoamTagMapper.class);
        List<Map> list = new ArrayList<>();
        if (scenicReleaseInfoList!=null){
            scenicReleaseInfoList.forEach((srInfo)->{
                if (srInfo == null){
                    return;
                }
                HashMap<String ,Object> map = new HashMap<>();
                Integer userId = srInfo.getUserId();
                RoamUserInfo userInfo = userMapper.FindOneUserByUid(userId);
                String headUrl="";
                String userName="";
                if (userInfo==null){
                    System.out.println(srInfo.getScenicReleaseId()+"为空");
                }else {
                    headUrl= userInfo.getUserHead();
                    userName= userInfo.getUserName();
                }
                map.put("headUrl",headUrl);
                map.put("userName",userName);
                Integer srid = srInfo.getScenicReleaseId();
                Integer commentCount= commentMapper.findCommentCountBySrid(srid);
                map.put("commentCount",commentCount);
                List<Integer> allTagBaySrid = tagMapper.findAllTagBaySrid(srInfo.getScenicReleaseId());
                List<String> tags=new ArrayList<>();
                allTagBaySrid.forEach(tagid->{
                    RoamTag oneTag = tagTextMapper.findOneTagByTagid(tagid);
                    tags.add(oneTag.getTagText());
                });
                map.put("tags",tags);
                String scenicReleaseText = srInfo.getScenicReleaseText();
                String htmlStr = scenicReleaseText;
                //<h1>青城 日子</h1><br><img src="http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userSpaceImg/20220322/434e599644974ee3814c65ea6d280d11.jpg" alt="cat" width="390&quot;&quot;">
                Set<String> pics = new HashSet<>();
                String img = "";
                Pattern p_image;
                Matcher m_image;
                //     String regEx_img = "<img.*src=(.*?)[^>]*?>"; //图片链接地址
                String regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>";
                p_image = Pattern.compile
                        (regEx_img, Pattern.CASE_INSENSITIVE);
                m_image = p_image.matcher(htmlStr);
                while (m_image.find()) {
                    // 得到<img />数据
                    img = m_image.group();
                    // 匹配<img>中的src数据
                    Matcher m = Pattern.compile("src\\s*=\\s*\"?(.*?)(\"|>|\\s+)").matcher(img);
                    while (m.find()) {
                        pics.add(m.group(1));
                    }
                }
                String[] urls = (String[]) pics.toArray(new String[pics.size()]);
                List newUrls = new ArrayList();

                if(pics.size()>3){
                    newUrls.addAll(Arrays.asList(urls).subList(0, 3));
                }else {
                    newUrls.addAll(Arrays.asList(urls));
                }
                map.put("pictures",newUrls);
                srInfo.setScenicReleaseText("");
                map.put("scenicReleaseInfo",srInfo);
                list.add(map);
            });
            return list;
        }
        return null;
    }
}
