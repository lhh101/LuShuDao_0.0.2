package com.lixiaohan.lushudao.util;

import com.lixiaohan.lushudao.pojo.ScenicReleaseInfo;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GetHtmlImgs {

    public static  List<String> getImgs(ScenicReleaseInfo srInfo){
        String scenicReleaseText = srInfo.getScenicReleaseText();
        String htmlStr = scenicReleaseText;
        //<h1>青城 日子</h1><br><img src="http://teaculture-001.oss-cn-chengdu.aliyuncs.com/userSpaceImg/20220322/434e599644974ee3814c65ea6d280d11.jpg" alt="cat" width="390&quot;&quot;">
        Set<String> pics = new HashSet<>();
        String img = "";
        Pattern p_image;
        Matcher m_image;
        //     String regEx_img = "<img.*src=(.*?)[^>]*?>"; //图片链接地址
        String regEx_img = "<img.*src\\s*=\\s*(.*?)[^>]*?>";
        p_image = Pattern.compile
                (regEx_img, Pattern.CASE_INSENSITIVE);
        m_image = p_image.matcher(htmlStr);
        while (m_image.find()) {
            // 得到<img />数据
            img = m_image.group();
            // 匹配<img>中的src数据
            Matcher m = Pattern.compile("src\\s*=\\s*\"?(.*?)(\"|>|\\s+)").matcher(img);
            while (m.find()) {
                pics.add(m.group(1));
            }
        }
        String[] urls =pics.toArray(new String[pics.size()]);
        List<String> newUrls = new ArrayList();

        if(pics.size()>3){
            newUrls.addAll(Arrays.asList(urls).subList(0, 3));
        }else {
            newUrls.addAll(Arrays.asList(urls));
        }
       return newUrls;
    }
}
