package com.lixiaohan.lushudao.util;

import com.lixiaohan.lushudao.controller.response.OffBBSResponse;
import com.lixiaohan.lushudao.dao.RoamCommentMapper;
import com.lixiaohan.lushudao.dao.SriGoodInfoMapper;
import com.lixiaohan.lushudao.pojo.ScenicReleaseInfo;
import com.lixiaohan.lushudao.pojo.SriGoodInfo;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class GetOffBBSResponseListFormSrList {
    public static List<OffBBSResponse> get(List<ScenicReleaseInfo> scenicReleaseList, Integer uid){
        SqlSession sqlSession = null;
        try{
            sqlSession = MybatisUtils.getSqlSession();
            RoamCommentMapper commentMapper = sqlSession.getMapper(RoamCommentMapper.class);
            SriGoodInfoMapper goodMapper = sqlSession.getMapper(SriGoodInfoMapper.class);
            List< OffBBSResponse> scenicWithPageList = scenicReleaseList.stream().map(srInfo -> {
                GetSrTags getSrTags = new GetSrTags();
                OffBBSResponse response = new OffBBSResponse();
                List<String> imgs = GetHtmlImgs.getImgs(srInfo);
                List<String> tagTextList = getSrTags.getTagTextList(srInfo.getScenicReleaseId());
                srInfo.setScenicReleaseText("");
                Integer commentCount= commentMapper.findCommentCountBySrid(srInfo.getScenicReleaseId());
                SriGoodInfo sriGoodInfo = goodMapper.seleStatueByUidAndSrid(srInfo.getScenicReleaseId(), uid);
                response.setSrInfo(srInfo);
                response.setImgs(imgs);
                response.setTagTexts(tagTextList);
                response.setComment(commentCount);
                if (!Objects.isNull(sriGoodInfo) && sriGoodInfo.getStatus().equals(1)){
                    response.setIsGood(Boolean.TRUE);
                }else {
                    response.setIsGood(Boolean.FALSE);
                }
                return response;
            }).collect(Collectors.toList());
            return scenicWithPageList;
        }catch (Exception e){
            throw e;
        }finally {
            if (sqlSession!=null)
            sqlSession.close();
        }

    }
}
