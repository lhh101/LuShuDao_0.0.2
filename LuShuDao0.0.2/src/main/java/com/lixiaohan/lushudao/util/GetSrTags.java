package com.lixiaohan.lushudao.util;

import com.lixiaohan.lushudao.dao.RoamReleaseTagMapper;
import com.lixiaohan.lushudao.dao.RoamTagMapper;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.stream.Collectors;

public class GetSrTags {


    public List<String> getTagTextList(Integer srid){
        SqlSession sqlSession = null;
        try{
            sqlSession = MybatisUtils.getSqlSession();
            RoamReleaseTagMapper releaseTagMapper = sqlSession.getMapper(RoamReleaseTagMapper.class);
            RoamTagMapper tagMapper = sqlSession.getMapper(RoamTagMapper.class);
            List<Integer> tagIdLsit = releaseTagMapper.findAllTagBaySrid(srid);
            if (tagIdLsit==null) return null;
            return tagIdLsit.stream().map(tagId -> tagMapper.findOneTagByTagid(tagId).getTagText()).collect(Collectors.toList());
        }catch (Exception e){
            throw e;
        }finally {
            if (sqlSession!=null)
            sqlSession.close();
        }



    }
}
